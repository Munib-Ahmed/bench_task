# Benchmark C++

clone this project into your system, through the following command;
```
git clone https://gitlab.com/Munib-Ahmed/bench_task.git
```

create a folder with name build and navigate to build directory, by entering following commands;
```
mkdir build 
cd build
```
now in order to run the program enter the following commands;
```
cmake ..
(in case you want to build in Debug mode, you can enter following command;
            cmake .. -DDEBUG=ON
)
cmake --build .
./bench
```
