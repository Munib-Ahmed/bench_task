#include <iostream>
#include <memory>
#include <vector>
#include <list>
#include <cstdlib>
#include <ctime>
#include <numeric>
#include "benchmark/benchmark.h"
using namespace std;

uint16_t sum_of_vector_squared(int num_entries)
{
  vector<uint16_t> vector_values;
  vector<uint16_t> vector_squared_values;
  srand(time(0));
  for (int i = 0; i < num_entries; i++)
  {
    vector_values.push_back(rand() % 100);
  }

  for (auto i = vector_values.begin(); i != vector_values.end(); ++i)
  {
    vector_squared_values.push_back((*i) * (*i));
  }
  uint16_t sum_of_elems = std::accumulate(vector_squared_values.begin(), vector_squared_values.end(), 0);
  return sum_of_elems;
}

uint16_t sum_of_list_squared(int num_entries)
{
  list<uint16_t> list_values;
  list<uint16_t> list_squared_values;
  srand(time(0));
  for (int i = 0; i < num_entries; i++)
  {
    list_values.push_back(rand() % 100);
  }

  for (auto i = list_values.begin(); i != list_values.end(); ++i)
  {
    list_squared_values.push_back((*i) * (*i));
  }
  uint16_t sum_of_elems = std::accumulate(list_squared_values.begin(), list_squared_values.end(), 0);
  return sum_of_elems;
}

static void vector_benchmark(benchmark::State &state)
{
  uint16_t sum_of_vector = 0;
  for (auto _ : state)
  {
    sum_of_vector = sum_of_vector_squared(state.range(0));
  }
  cout << "sum of vector = " << sum_of_vector << endl;
}
BENCHMARK(vector_benchmark)->Arg(2)->Iterations(1);

static void list_benchmark(benchmark::State &state)
{
  uint16_t sum_of_list = 0;
  for (auto _ : state)
  {
    sum_of_list = sum_of_list_squared(state.range(0));
  }
  cout << "sum of list = " << sum_of_list << endl;
}
BENCHMARK(list_benchmark)->Arg(2)->Iterations(1);

BENCHMARK_MAIN();